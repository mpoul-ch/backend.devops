echo "Adding vhost $2.$1.dev"

cd $1
docker-compose down

sed -i '' "s/VIRTUAL_HOST=.*/VIRTUAL_HOST=$1,admin.$1,api.$1,front.$1,$2.$1/" .env
 
#sed -i '' "s/yii2advanced/$CDBNAME/" $1/environments/dev/common/config/main-local
cp conf/fpm.tpl.orig conf/fpm.tpl
sed -i '' "s/server_name frontend.test/server_name $2.$1/" conf/fpm.tpl
sed -i '' "s/frontend\/web/$3/" conf/fpm.tpl
sed -i '' "s/frontend\/web/$3/" conf/fpm.tpl
sed -i '' "s/frontend-access.log/$2-access.log/" conf/fpm.tpl
sed -i '' "s/frontend-error.log/$2-error.log/" conf/fpm.tpl
 cat conf/fpm.tpl >> conf/nginx-site.conf

#cat conf/nginx-site.conf conf/fpm.tpl >> conf/nginx-site.conf
 
#        access_log  /var/www/log/frontend-access.log;
#        error_log   /var/www/log/frontend-error.log;

#cd $1

#echo "127.0.0.1 $2.$1" >> /etc/hosts
#docker-compose run --rm backend composer install
#docker-compose run --rm backend /app/init
#docker-compose run --rm backend yii migrate
docker-compose up -d
echo "Welcome to docker container $1"
docker exec -it $1 bash

#conf/nginx-site.conf
