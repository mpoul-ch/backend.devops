#!/bin/sh
echo "Adding vhost $1.dev"
cp -R -p template $1
#cd $1
CDBNAME=${1//./_}
echo $CDBNAME
docker exec -it mariadb mysql -uroot -e 'create database '$CDBNAME';'


sed -i '' "s/VIRTUAL_HOST=.*/VIRTUAL_HOST=$1,admin.$1,api.$1,front.$1/" $1/.env
sed -i '' "s/VIRTUAL_HOST_CN=.*/VIRTUAL_HOST_CN=$1/" $1/.env 
#sed -i '' "s/yii2advanced/$CDBNAME/" $1/environments/dev/common/config/main-local

sed -i '' "s/server_name frontend.test/server_name $1/" $1/conf/nginx-site.conf
sed -i '' "s/server_name backend.test/server_name admin.$1/" $1/conf/nginx-site.conf
sed -i '' "s/server_name api.test/server_name api.$1/" $1/conf/nginx-site.conf
sed -i '' "s/server_name node.test/server_name front.$1/" $1/conf/nginx-site.conf

cd $1
echo "127.0.0.1 $1 api.$1 admin.$1 front.$1" >> /etc/hosts
#docker-compose run --rm backend composer install
#docker-compose run --rm backend /app/init
#docker-compose run --rm backend yii migrate
docker-compose up -d
echo "Welcome to docker container $1"
docker exec -it $1 bash

#conf/nginx-site.conf
