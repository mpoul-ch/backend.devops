#!/bin/sh
#DOCKER_DNS_HOST=`ifconfig docker0 | grep "inet" | head -n1 | awk '{ print $2}' | cut -d: -f2`
#echo "nameserver $DOCKER_DNS_HOST" >> /etc/resolv.conf
#echo "DOCKER_OPTS=\"--dns $DOCKER_DNS_HOST --dns 8.8.8.8 --dns 8.8.4.4\"" >> /etc/default/docker
docker network create ngproxy
docker-compose up -d
