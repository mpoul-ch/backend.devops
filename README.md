**Установка окружения для backend разработки **

Данная сборка предназначена для развертывания локальных виртуальных хостов для разработчиков студии Олега Чулакова.

*В сборку входит 2 постоянных контейнера(nginx-proxy и mariadb) и шаблонный контейнер для разработки*
Контейнер для разработки адаптирован для проектов YII2 (Backend) и NodeJS (Frontend). 
В контейнере для разаработки имеется 4 виртуальных сервера 
1. your.dev.domain >> document root path /var/www/html/frontend/web/
2. admin.your.dev.domain >> document root path /var/www/html/backend/web/
3. api.your.dev.domain >> document root path /var/www/html/api/web/
4. front.your.dev.domain >> document root path /var/www/html/public/
После создания контейнера создается каталог *your.dev.domain* и база данных *your_dev_domain* в контейнере mariadb, а также вносятся записи в файл /etc/hosts (127.0.0.1 your.dev.domain admin.your.dev.domain api.your.dev.domain front.your.dev.domain)

Конетейнер nginx-proxy автоматически регистрирует новые проекты и проксирует запросы с созданные контейнеры для разработчиков. 
Данный контейнер слушает порт **8082**, соответственно ваши проекты будут доступны по адресам:
1. http://your.dev.domain:8082/ >> document root path /var/www/html/frontend/web/
2. http://admin.your.dev.domain:8082/ >> document root path /var/www/html/backend/web/
3. http://api.your.dev.domain:8082/ >> document root path /var/www/html/api/web/
4. http://front.your.dev.domain:8082/ >> document root path /var/www/html/public/

Контейнер mariadb нутри докер сети имеет хост mariadb, слушает внутренний прот **3306** и внешний **33306** (В конфигурации проекта оставляем внутренний порт, для доступа к БД с хостовой машины указываем внешний порт) 



---

## Особенности устнаовки и настройки Docker на MacOS

Для корректной работы Docker необходима поддержка процессором инструкции HV.
Убедитесь что процессор Вашего Mac поддерживает инструкции виртуализации HV выполнив команду в онсоли sysctl kern.hv_support 
Если вывод kern.hv_support: 1 устанавливаем Docker https://hub.docker.com/editions/community/docker-ce-desktop-mac .
Если вывод kern.hv_support: 0 устанавливаем Docker-Toolbox https://docs.docker.com/toolbox/toolbox_install_mac/ 

**Важно!!!** если вы запускаете контенеры и сборки контейнеров вне домашней директории, в настройках докер демона необходимо добавить пути к каталогу backend.devops.
Для этого в небоходимо кликнуть по значку докер в трее  -> Preferences -> File Sharing и указать путь к каталогу backend.devops

---

## Настройка контейнеров

После клонирования необходимо прописать переменные в файл .env , а также раскоментировать нужные опции в файле docker-compose.yml
Для запуска nginx-proxy и mariadb выполните в консоли $ sh start-proxy.sh 


---

## Создание контейнера с виртуальным хостом
**!!! Внимание!!! контейнеры мотрутруют ssh ключи из вашей домашней директории. Во избежание инцидентов связнных в созможной перезаписью ключей рекомендую сделать резервную копию каталога ~/.ssh**

1. Перед созданием контейнера виртуального хоста необходимо прописать переменные в шаблоне **tempyii/.env** и раскомментировать необходимые опции в файле **tempyii/docker-compose.yml**. 
2. Для создания контейнера с проектом выполните команду указав dns вашего проекта **sudo sh add-new-host.sh dns.name.your.project**. Данный скрипт создаст каталог соответсвующий заданому dns (dns.name.your.project) и базу данных dns_name_your_project 
3. Поместите исходный код Вашего проекта в каталог **dns.name.your.project/src/frontend** . По умолчанию создается конфигурация nginx имеющая три NS - dns.name.your.project имеющий WEBROOT /var/www/html/frontend , admin.dns.name.your.project имеющий WEBROOT /var/www/html/backend и api.dns.name.your.project имеющий WEBROOT /var/www/html/api
4. Для получения доступа к CLI контейнера выполните команду **sh doexec *dns.name.your.project** или **docker exec -it *dns.name.your.project /bin/bash**

## Запуск и остановка контейнеров/Доступ в командную строку контейнера
Для запуска контейнеров nginx-proxy и mariadb в терминале перейдите в каталог с backend.devops  и выполните команду docker-compose up -d (docker-compose down - остановка контейнеров)
Для запуска контейнеров для разработки в терминале перейдите в каталог с backend.devops/your.dev.domain и выполните команду docker-compose up -d (docker-compose down - остановка контейнеров)
Для доступа к командной строки контейнера выпонлните команду **docker exec -it your.dev.domain /bin/bash** 





